(function($){
Drupal.behaviors.TodoFilter = {
  attach: function (context, settings) {
    $(context).find('.todo-check').click(function(event) {
      var _this = $(this);
      var instance = _this.data('todo-task');
      _this.parent().toggleClass('checked');
      if (typeof(instance) != 'undefined') {
        $.post(Drupal.settings.basePath + 'todo/toggle/' + instance);
      }
    });
  }
};
})(jQuery);
